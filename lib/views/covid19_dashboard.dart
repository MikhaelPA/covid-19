import 'package:Covid_19/models/api_response.dart';
import 'package:Covid_19/models/country_covid19.dart';
import 'package:Covid_19/models/global_covid19.dart';
import 'package:Covid_19/services/covid19_service.dart';
import 'package:Covid_19/views/country_search.dart';
import 'package:Covid_19/views/country_view.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
//import 'package:url_launcher/url_launcher.dart';

import 'global_view.dart';

class Covid19Dashboard extends StatefulWidget {
  @override
  _Covid19DashboardState createState() => _Covid19DashboardState();
}

class _Covid19DashboardState extends State<Covid19Dashboard> {
  Covid19Service get service => GetIt.I<Covid19Service>();

  APIResponse<GlobalCovid19> _apiResponseGlobal;
  // APIResponse<CountryCovid19> _apiResponseCountry;
  APIResponse<List<CountryCovid19KeyValue>> _apiResponseCountry;
  List<String> _countries = [];
  bool _isLoading = false;
  int _currentIndex = 0;
  static String _appTitle = 'COVID-19';
  Widget _appBarTitle = Text(_appTitle);
  Icon _searchIcon = Icon(Icons.search);
  String _query = '';
  @override
  void initState() {
    _fetchData();
    super.initState();
  }

  void _fetchData() async {
    setState(() {
      _isLoading = true;
    });

    _apiResponseGlobal = await service.getGlobalData();
    _apiResponseCountry = await service.getCountriesData();

    //await service.getCountriesData();getCountryData

    _buildCountries();
    setState(() {
      _isLoading = false;
    });
  }

  void _refreshData() {
    _fetchData();
  }

  void _buildCountries() {
    if (_apiResponseCountry.error == false) {
      final countries = _apiResponseCountry.data.map((item) {
        return item.countryName;
      }).toList();
      setState(() {
        _query = '';
        _countries = countries;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final tabs = [
      _buildGlobal(),
      _buildCountry(),
      _disclaimer(),
    ];

    return Scaffold(
      appBar: AppBar(
        leading: _currentIndex == 2
            ? Icon(Icons.info_outline)
            : IconButton(
                icon: Icon(Icons.refresh),
                onPressed: _refreshData,
              ),
        title: _appBarTitle,
        actions: _currentIndex == 1
            ? <Widget>[
                IconButton(
                  icon: _searchIcon,
                  onPressed: () async {
                    final String selected = await showSearch(
                      context: context,
                      delegate: CountrySearch(_countries, _apiResponseCountry),
                    );
                    if (selected != null && selected != _query) {
                      setState(() {
                        _query = selected;
                      });
                    }
                  }, //_searchData,
                )
              ]
            : [], //only country tab has search
      ),
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 30.0,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text('Global'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pin_drop),
            title: Text('Country'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.info),
            title: Text('Info'),
          )
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }

  Widget _buildGlobal() {
    return Builder(
      builder: (_) {
        if (_isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (_apiResponseGlobal.error) {
          return Center(
            child: Text(_apiResponseGlobal.errorMessage),
          );
        }

        return GlobalView(_apiResponseGlobal);
      },
    );
  }

  Widget _buildCountry() {
    return Builder(
      builder: (_) {
        if (_isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (_apiResponseCountry.error) {
          return Center(
            child: Text(_apiResponseCountry.errorMessage),
          );
        }
        return CountryView(_apiResponseCountry, _query);
      },
    );
  }

  Widget _disclaimer() {
    return Card(
      margin: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Author'),
            subtitle: Text('Mikhael Pramodana Agus'),
          ),
          ListTile(
            leading: Icon(Icons.email),
            title: Text('Email'),
            subtitle: Text('mikhael.science@gmail.com'),
          ),
          ListTile(
            leading: Icon(Icons.code),
            title: Text('Git'),
            subtitle: Text('gitlab.com/MikhaelPA/covid-19.git'),
          ),
          ListTile(
            leading: Icon(Icons.web),
            title: Text('Information from'),
            subtitle: Text('www.worldometers.info/coronavirus'),

            // subtitle: InkWell(
            //     child: new Text('www.worldometers.info'),
            //     onTap: () async =>
            //         await launch('https://www.worldometers.info/coronavirus/')),
          ),
        ],
      ),
    );
  }
}
