import 'package:Covid_19/models/api_response.dart';
import 'package:Covid_19/models/global_covid19.dart';
import 'package:flutter/material.dart';

class GlobalView extends StatelessWidget{
  final _styleTitleFont = const TextStyle(fontSize: 20.0, color: Colors.white);
  final _styleSubsFont = const TextStyle(fontSize: 45.0, color: Colors.white);
  static const _iconSize = 55.0;

final APIResponse<GlobalCovid19> apiResponse;
GlobalView(this.apiResponse);

@override
  Widget build(BuildContext context) {
    return ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(15.0),
              color: Colors.blue,
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
                leading: Icon(
                  Icons.vpn_lock,
                  color: Colors.white,
                  size: _iconSize,
                ),
                title: Text(
                  'Global Cases',
                  style: _styleTitleFont,
                ),
                subtitle: Text(
                  apiResponse.data.casesStr,
                  style: _styleSubsFont,
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(15.0),
              color: Colors.redAccent,
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
                leading: Icon(
                  Icons.group,
                  color: Colors.white,
                  size: _iconSize,
                ),
                title: Text(
                  'Deaths',
                  style: _styleTitleFont,
                ),
                subtitle: Text(
                  apiResponse.data.deathsStr,
                  style: _styleSubsFont,
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(15.0),
              color:Colors.greenAccent,
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
                leading: Icon(
                  Icons.verified_user,
                  color: Colors.white,
                  size: _iconSize,
                ),
                title: Text(
                  'Recovered',
                  style: _styleTitleFont,
                ),
                subtitle: Text(
                  apiResponse.data.recoveredStr,
                  style: _styleSubsFont,
                ),
              ),
            )
          ],
        );
  }
}