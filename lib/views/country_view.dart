import 'package:Covid_19/models/api_response.dart';
import 'package:Covid_19/models/country_covid19.dart';
import 'package:flutter/material.dart';

class CountryView extends StatelessWidget {
  final _styleTitleFont =
      const TextStyle(fontSize: 30.0, color: Colors.blueGrey, fontWeight: FontWeight.bold);
  final _styleSubsFont =
      const TextStyle(fontSize: 20.0, color: Colors.blueGrey);

  // final APIResponse<CountryCovid19> apiResponse;
  final APIResponse<List<CountryCovid19KeyValue>> apiResponse;
  final String query;
  CountryView(this.apiResponse, this.query);

  Widget _buildRow(CountryCovid19KeyValue pair) {
    return Card(
      margin: const EdgeInsets.all(10.0),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
        title: Text(
          pair.countryName, //_selected.countryName,
          style: _styleTitleFont,
        ),
        subtitle: Text(
          'Total Cases: ${pair.countryData.casesStr}\n'
          'New Cases: ${pair.countryData.todayCasesStr}\n'
          'Total Deaths: ${pair.countryData.deathsStr}\n'
          'New Deaths: ${pair.countryData.todayDeathsStr}\n'
          'Total Recovered: ${pair.countryData.recoveredStr}\n'
          'Active Cases: ${pair.countryData.activeStr}',
          style: _styleSubsFont,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final queriedCountry = query.isEmpty
            ? apiResponse.data
            : apiResponse.data
                .where((item) => item.countryName
                    .toLowerCase()
                    .startsWith(query.toLowerCase()))
                .toList();
    return
        ListView.builder(
            //padding: const EdgeInsets.all(16.0),
            itemCount: queriedCountry.length,
            itemBuilder: (context, i) {
              return _buildRow(queriedCountry[i]);
            });
  }
}
