import 'package:Covid_19/models/api_response.dart';
import 'package:Covid_19/models/country_covid19.dart';
import 'package:Covid_19/views/country_view.dart';
import 'package:flutter/material.dart';

class CountrySearch extends SearchDelegate<String> {
  final List<String> countryNames;

  final APIResponse<List<CountryCovid19KeyValue>> apiResponse;
  CountrySearch(this.countryNames, this.apiResponse);
  
  final TextStyle _textStyleHighlight = TextStyle(
      color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 20.0);
  final TextStyle _textStyle = TextStyle(color: Colors.grey, fontSize: 18.0);
  List<String> _history = ['Indonesia', 'Singapore'];

  @override
  List<Widget> buildActions(BuildContext context) {
    // Clear text
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
          showSuggestions(context);
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Close search
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return CountryView(apiResponse, query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // This method is called everytime the search term changes.
    // If you want to add search suggestion as the user enters their search term
    final suggestions = query.isEmpty
        ? _history
        : countryNames.where((c) => c.toLowerCase().startsWith(query)).toList();

    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) {
        return ListTile(
          //title: Text(suggestions[index]),
          leading: Icon(Icons.star),
          onTap: () {
            // Add queried country to history if it doesn't exist.
            if (!_history.contains(suggestions[index])) {
              _history.add(suggestions[index]);
            }
            showResults(context);
            // the result thrown to original caller of showResult.
            close(context, suggestions[index]);
          },
          title: RichText(
            text: TextSpan(
                text: suggestions[index].substring(0, query.length),
                style: _textStyleHighlight,
                children: [
                  TextSpan(
                      text: suggestions[index].substring(query.length),
                      style: _textStyle)
                ]),
          ),
        );
      },
    );
  }
}
