import 'dart:convert';

import 'package:intl/intl.dart';

class CountryCovid19KeyValue {
  String countryName;
  CountryCovid19 countryData;

  CountryCovid19KeyValue({this.countryName, this.countryData});
}

CountryCovid19 countryFromJson(String str) {
  final jsonData = json.decode(str);
  return CountryCovid19.fromJson(jsonData);
}

class CountryCovid19 {
  final _numFormat = new NumberFormat('#,###', 'en_US');

  String country;
  int cases;
  int todayCases;
  int deaths;
  int todayDeaths;
  int recovered;
  int active;
  int critical;

  String get casesStr => _numFormat.format(cases);
  String get todayCasesStr => _numFormat.format(todayCases);
  String get deathsStr => _numFormat.format(deaths);
  String get todayDeathsStr => _numFormat.format(todayDeaths);
  String get recoveredStr => _numFormat.format(recovered);
  String get activeStr => _numFormat.format(active);
  String get criticalStr => _numFormat.format(critical);

  CountryCovid19(
      {this.country,
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical});

  factory CountryCovid19.fromJson(Map<String, dynamic> json) {
    return new CountryCovid19(
        country: json['country'],
        cases: json['cases'],
        todayCases: json['todayCases'],
        deaths: json['deaths'],
        todayDeaths: json['todayDeaths'],
        recovered: json['recovered'],
        active: json['active'],
        critical: json['critical']);
  }
}
