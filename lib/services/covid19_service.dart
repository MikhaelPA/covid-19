import 'dart:convert';

import 'package:Covid_19/models/api_response.dart';
import 'package:Covid_19/models/country_covid19.dart';
import 'package:Covid_19/models/global_covid19.dart';
import 'package:http/http.dart' as http;

class Covid19Service {
  static const API = 'https://coronavirus-19-api.herokuapp.com';

  Future<APIResponse<GlobalCovid19>> getGlobalData() {
    return http.get(API + '/all').then((data) {
      if (data.statusCode == 200) {
        final globalCovid = globalFromJson(data.body);

        return APIResponse<GlobalCovid19>(data: globalCovid);
      }
      return APIResponse<GlobalCovid19>(
          error: true, errorMessage: 'An Error Occured');
    }).catchError((_) => APIResponse<GlobalCovid19>(
        error: true, errorMessage: 'An Error Occured'));
  }

  Future<APIResponse<CountryCovid19>> getCountryData() {
    return http.get(API + '/countries/indonesia').then((data) {
      if (data.statusCode == 200) {
        final country = countryFromJson(data.body);
        return APIResponse<CountryCovid19>(data: country);
      }
      return APIResponse<CountryCovid19>(
          error: true, errorMessage: 'An Error Occured');
    }).catchError((_) {
      return APIResponse<CountryCovid19>(
          error: true, errorMessage: 'An Error Occured');
    });
  }

  Future<APIResponse<List<CountryCovid19KeyValue>>> getCountriesData() {
    return http.get(API + '/countries').then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        List<CountryCovid19KeyValue> countries = <CountryCovid19KeyValue>[];
        for (var item in jsonData) {
          final country = CountryCovid19KeyValue(
              countryName: item['country'],
              countryData: CountryCovid19.fromJson(item));
          countries.add(country);
        }
        return APIResponse<List<CountryCovid19KeyValue>>(data: countries);
      }
      return APIResponse<List<CountryCovid19KeyValue>>(
          error: true, errorMessage: 'An Error Occured');
    }).catchError((_) {
      print(_);
      return APIResponse<List<CountryCovid19KeyValue>>(
          error: true, errorMessage: 'An Error Occured');
    });
  }
}
