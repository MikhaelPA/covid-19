import 'package:Covid_19/services/covid19_service.dart';
import 'package:Covid_19/views/covid19_dashboard.dart';
//import 'package:Covid_19/views/randomwords.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

// void main() => runApp(Covid19App());

void setupLocator(){
  GetIt.I.registerLazySingleton(() => Covid19Service());
}

void main(){
  setupLocator();
  runApp(Covid19App());
}

class Covid19App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
        title: 'COVID-19',
        home: Covid19Dashboard(),//RandomWords(),
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          
          primaryColor: Colors.blue,
          //fontFamily: 'Frutiger'
          

        ),
        //home: MyHomePage(title: 'Flutter Demo Home Page'),//MyHomePage is a defined class
        // home: Scaffold(
        //   appBar: AppBar(
        //     title: Text('Welcome To Flutter'),
        //   ),
        //   body: Center(
        //     child: RandomWords(),
        //   ),
        // )
        );
  }
}

